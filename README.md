# A Guided Tour of LibertyListView #
This code-base is designed as an introduction to the ListView in Xamarin.Forms. Look at comments throughout the master branch to see explanations. Follow along with the guided tour below to understand the function provided by each component of the app. There are a few branches to be aware of: 

* master:  The most up-to-date code, with the most explanatory comments and the most implementation.
* ilist:  This branch shows why an iList<T> is not sufficient... This is why the ObservableCollection<T> exists.
* smackdown:  Demonstrates INotifyPropertyChanged vs. INotifyCollectionChanged.
* classSessionOneComplete:  Stopping point for class session number one on ListViews.
* classTwoComplete:  
	* Implementation of SelectedItem binding.
	* Implementation of Prism's EventToCommandBehavior to wire up the ListView's ItemSelected and ItemTapped events to ViewModel commands (PersonSelectedCommand and PersonTappedCommand, respectively).
	* Implementation of pull-to-refresh functionality.
* contextActions:  Implementation of three context actions on ListView items.

## App.xaml.cs
* Very little new here… except for the Repository. This is the most basic starter implementation of the Repository pattern one could possibly have.

## MainPageViewModel.cs
* ObservableCollection of People.
* A bool to tell the view when to display a busy spinner.
* Data is fetched in the constructor. We’ll get into how to refresh this data in the near future.
* Several delegate commands hooked up to the view:  
	* AddPersonCommand triggered by the add button in the header of the ListView.
	* PersonTappedCommand triggered by the ItemTapped event of the ListView thanks to Prism's [EventToCommandBehavior](http://prismlibrary.github.io/docs/xamarin-forms/EventToCommandBehavior.html).
	* PersonSelectedCommand triggered by the ItemSelected event of the ListView thanks to Prism's [EventToCommandBehavior](http://prismlibrary.github.io/docs/xamarin-forms/EventToCommandBehavior.html).
	* PullToRefreshCommand triggered by pulling down on the ListView to fetch fresh data from whatever data source the ListView is hooked up to.
	* MoreCommand triggered by a ContextAction attached to ListView items.
	* DeleteCommand triggered by a ContexAction attached to ListView items.
* OnNavigatingTo:  Checks parameters for a particular key that indicates there is a new person to add to the list.

## AddPersonPageViewModel.cs
* Page for adding new people to the list.
* Launched as a modal page.
* Has entry fields to populate a new Person object.
* A bool to tell view when to display a busy spinner.
* Two commands:
	* SaveCommand:  Save the new person and pass it back to the calling page.
	* CancelCommand:  Abort the current add operation, and return to the calling page.

## MainPage.xaml
* Home of the ListView for this app.
* Comments explain the use of the ShowIsBusySpinner property.
* Comments explain how the BindingContext of a listView item is a special case, in which the UI control does not inherit its parent’s BindingContext.
* Includes a header/footer for ListView. Not required, but convenient to use. This is where our button for adding a new person lives.
* Makes use of the Prism.Behaviors xaml namespace (xmlns) to leverage Prism's [EventToCommandBehavior](http://prismlibrary.github.io/docs/xamarin-forms/EventToCommandBehavior.html).

## AddPersonPage.xaml
* Simple entry page to enter data to create a new person for the People list.
* Demonstrates use of negative margins.
* Demonstrates two ways to add spacers between various UI elements:  Using margins and using a transparent BoxView.

## Person.cs
* Simple model class for a single person.

## Repository.cs : IRepository.cs
* Extremely basic data layer used to get and add people.

## NavParameterKeys.cs
* Constants class to prevent fat-fingering this key throughout the code.

## CustomExtensionMethods.cs
* Completely unnecessary class, but it helps make the debug output very easy to read and understand.
* You could make an “extension method” like this for any class you wanted, if need be.

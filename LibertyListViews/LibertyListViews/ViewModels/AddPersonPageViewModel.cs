﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using LibertyListViews.Helpers;
using LibertyListViews.Models;
using LibertyListViews.Services;
using Prism.Commands;
using Prism.Navigation;

namespace LibertyListViews.ViewModels
{
    public class AddPersonPageViewModel : ViewModelBase
    {
        IRepository _repository;

        public DelegateCommand CancelCommand { get; set; }
        public DelegateCommand SaveCommand { get; set; }

        //Note:  Property bound to first name entry.
        private string _firstNameField;
        public string FirstNameField
        {
            get { return _firstNameField; }
            set { SetProperty(ref _firstNameField, value); }
        }

        //Note:  Property bound to last name entry.
        private string _lastNameField;
        public string LastNameField
        {
            get { return _lastNameField; }
            set { SetProperty(ref _lastNameField, value); }
        }

        //Note:  This bool controls when the busy spinner is shown.
        private bool _showIsBusySpinner;
        public bool ShowIsBusySpinner
        {
            get { return _showIsBusySpinner; }
            set { SetProperty(ref _showIsBusySpinner, value); }
        }

        public AddPersonPageViewModel(INavigationService navigationService, IRepository repository)
            : base(navigationService)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(AddPersonPageViewModel)}:  ctor");

            _repository = repository;

            CancelCommand = new DelegateCommand(OnCancel);
            SaveCommand = new DelegateCommand(OnSave);
        }

        /// <summary>
        /// Save the new person to the repository, which takes care of saving the new person to whatever our
        /// data source is.
        /// Ideally, there should be some validation in this method to prevent saving a new person with a blank 
        /// first or last name.
        /// </summary>
        private async void OnSave()
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnSave)}");

            Person newPerson = new Person
            {
                FirstName = this.FirstNameField,
                LastName = this.LastNameField
            };

            ShowIsBusySpinner = true;
            await _repository.AddPerson(newPerson);
			ShowIsBusySpinner = false;

            var navParams = new NavigationParameters();
            navParams.Add(NavParameterKeys.ADD_NEW_PERSON_KEY, newPerson);

            //Note:  The line below is a hack you *should* not need... But might need anyway!
            // Without this tiny delay, the busy spinner will just stay spinning forever.
            await Task.Delay(1);

            await _navigationService.GoBackAsync(navParams);
        }

        /// <summary>
        /// Simply navigate back to the calling page.
        /// </summary>
        private async void OnCancel()
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnCancel)}");
            await _navigationService.GoBackAsync();
        }
    }
}

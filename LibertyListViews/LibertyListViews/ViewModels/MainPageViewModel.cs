﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using LibertyListViews.Models;
using LibertyListViews.Services;
using System.Diagnostics;
using LibertyListViews.Views;
using System.Threading.Tasks;
using System.Collections.Specialized;
using LibertyListViews.Helpers;
using System.ComponentModel;

namespace LibertyListViews.ViewModels
{
    public class MainPageViewModel : ViewModelBase
    {
        IRepository _repository;

        public DelegateCommand AddPersonCommand { get; set; }
		public DelegateCommand PullToRefreshCommand { get; set; }
        public DelegateCommand<Person> PersonTappedCommand { get; set; }
        public DelegateCommand<Person> PersonSelectedCommand { get; set; }
        public DelegateCommand<Person> InfoCommand { get; set; }
        public DelegateCommand<Person> DeleteCommand { get; set; }
        public DelegateCommand<Person> PhotosCommand { get; set; }

        //Note:  This is bound to the ItemsSource for the ListView on MainPage.
        private ObservableCollection<Person> _people;
        public ObservableCollection<Person> People
        {
            get { return _people; }
            set { SetProperty(ref _people, value); }
        }

        //Note:  Bound to both the ContentPage's AND the ListView's busy properties:  IsBusy is the
        //          property for the ContentPage, and IsRefreshing is the property for the ListView.
        private bool _showIsBusySpinner;
        public bool ShowIsBusySpinner
        {
            get { return _showIsBusySpinner; }
            set { SetProperty(ref _showIsBusySpinner, value); }
        }

        //Note:  This is bound to the currently selected person in the ListView.
        private Person _selectedPerson;
        public Person SelectedPerson
        {
            get { return _selectedPerson; }
            set { SetProperty(ref _selectedPerson, value); }
        }

        public MainPageViewModel(INavigationService navigationService, IRepository repository)
            : base(navigationService)
        {
            _repository = repository;
            Title = "Party People";

            AddPersonCommand = new DelegateCommand(OnAddPerson);
			PullToRefreshCommand = new DelegateCommand(OnPullToRefresh);
            PersonTappedCommand = new DelegateCommand<Person>(OnPersonTapped);
            PersonSelectedCommand = new DelegateCommand<Person>(OnPersonSelected);
            InfoCommand = new DelegateCommand<Person>(OnInfoTapped);
            DeleteCommand = new DelegateCommand<Person>(OnDeleteTapped);
            PhotosCommand = new DelegateCommand<Person>(OnPhotosTapped);

            RefreshPeopleList();
        }

        private void OnPhotosTapped(Person personTapped)
        {
            string selectedPersonString = SelectedPerson == null ? "null" : SelectedPerson.ToString();
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnPhotosTapped)}:  {personTapped}\n\tFYI, the SelectedPerson is {selectedPersonString}");
        }

        private void OnDeleteTapped(Person personToDelete)
        {
            string selectedPersonString = SelectedPerson == null ? "null" : SelectedPerson.ToString();
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnDeleteTapped)}:  {personToDelete}\n\tFYI, the SelectedPerson is {selectedPersonString}");
        }

        private void OnInfoTapped(Person personTapped)
        {
            string selectedPersonString = SelectedPerson == null ? "null" : SelectedPerson.ToString();
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnInfoTapped)}:  {personTapped}\n\tFYI, the SelectedPerson is {selectedPersonString}");
        }

        /// <summary>
        /// Executed when the user pulls down on the ListView to refresh the data being displayed.
        /// </summary>
        private async void OnPullToRefresh()
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnPullToRefresh)}");

            await RefreshPeopleList();
        }

        /// <summary>
        /// Executed when the ItemSelected event is raised on the associated ListView in the MainPage.
        /// Remember that the ItemSelected event only gets raised if the item that is tapped on is not
        /// the currently selected item.
        /// </summary>
        /// <param name="personSelected">Person selected.</param>
        private void OnPersonSelected(Person personSelected)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnPersonSelected)}:  {personSelected}");
        }

        /// <summary>
        /// Executed when the ItemTapped event is raised on the associated ListView in the MainPage.
        /// The ItemTapped event is raised every single time a ListView item is tapped, whether it is
        /// the currently selected item or not.
        /// </summary>
        /// <param name="personTapped">Person tapped.</param>
        private void OnPersonTapped(Person personTapped)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnPersonTapped)}:  {personTapped}");
        }

        /// <summary>
        /// Launch the AddPerson page as a modal view.
        /// </summary>
        private async void OnAddPerson()
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnAddPerson)}");
            await _navigationService.NavigateAsync(nameof(AddPersonPage), null, true, true);
        }

        /// <summary>
        /// Check the parameters for a new person key and add to list if there is one.
        /// </summary>
        public override async void OnNavigatingTo(NavigationParameters parameters)
        {
            base.OnNavigatingTo(parameters);
            if (parameters != null && parameters.ContainsKey(NavParameterKeys.ADD_NEW_PERSON_KEY))
            {
                var personToAdd = (Person)parameters[NavParameterKeys.ADD_NEW_PERSON_KEY];
                People.Add(personToAdd);
            }
        }

        /// <summary>
        /// Gets the list of people from our data source.
        /// </summary>
        private async Task RefreshPeopleList()
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(RefreshPeopleList)}");

            ShowIsBusySpinner = true;
            SelectedPerson = null;
            var listOfPeople = await _repository.GetPeople();
            People = new ObservableCollection<Person>(listOfPeople);
            ShowIsBusySpinner = false;
        }
    }
}

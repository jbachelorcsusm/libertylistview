﻿using Prism;
using Prism.Ioc;
using LibertyListViews.ViewModels;
using LibertyListViews.Views;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Prism.Unity;
using LibertyListViews.Services;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace LibertyListViews
{
    public partial class App : PrismApplication
    {
        /* 
         * The Xamarin Forms XAML Previewer in Visual Studio uses System.Activator.CreateInstance.
         * This imposes a limitation in which the App class must have a default constructor. 
         * App(IPlatformInitializer initializer = null) cannot be handled by the Activator.
         */
        public App() : this(null) { }

        public App(IPlatformInitializer initializer) : base(initializer) { }

        protected override async void OnInitialized()
        {
            InitializeComponent();

            await NavigationService.NavigateAsync($"{nameof(NavigationPage)}/{nameof(MainPage)}");
        }

        protected override void RegisterTypes(IContainerRegistry containerRegistry)
        {
            //Note:  Standard page registrations, mapping view to viewModel for all custom pages:
            containerRegistry.RegisterForNavigation<NavigationPage>();
            containerRegistry.RegisterForNavigation<MainPage, MainPageViewModel>();
            containerRegistry.RegisterForNavigation<AddPersonPage, AddPersonPageViewModel>();

            //Note:  This is a very basic example of a data layer for an app:
            containerRegistry.RegisterSingleton<IRepository, Repository>();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using LibertyListViews.Models;

namespace LibertyListViews.Services
{
    public class Repository : IRepository
    {
        /// <summary>
        /// The people fetched from some data source, normally. For this example, we'll just use
        /// some hard-coded data.
        /// </summary>
        IList<Person> peopleFromSomeDataSource = null;

        public Repository()
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(Repository)}:  ctor");
        }

        // Method summary provided in interface.
        public async Task<IList<Person>> GetPeople()
        {
            if (peopleFromSomeDataSource == null)
            {
                peopleFromSomeDataSource = GetCannedData();
            }

            // Let's pretend this is calling out to a web service of some sort to get the data, 
            // so it will take some time...
            await Task.Delay(2000);

            return peopleFromSomeDataSource;
        }

        /// <summary>
        /// Hard-coded data to seed our list of People.
        /// </summary>
        /// <returns>Pre-made data.</returns>
        private IList<Person> GetCannedData()
        {
            var peopleList = new List<Person>();
            peopleList.Add(new Person { FirstName = "Jon", LastName = "Bachelor" });
            peopleList.Add(new Person { FirstName = "Neil", LastName = "Peart" });
            peopleList.Add(new Person { FirstName = "Geddy", LastName = "Lee" });
            peopleList.Add(new Person { FirstName = "Roxy", LastName = "Bachelor" });
            peopleList.Add(new Person { FirstName = "Alex", LastName = "Lifeson" });
            peopleList.Add(new Person { FirstName = "Trent", LastName = "Reznor" });
            return peopleList;
        }

        // Method summary provided in interface.
        public async Task<IList<Person>> GetPeople(int numberOfPeople)
        {
            peopleFromSomeDataSource = new List<Person>();

            for (int i = 0; i < numberOfPeople; i++)
            {
                var newPerson = new Person()
                {
                    FirstName = $"First-{i}",
                    LastName = $"Last-{i}"
                };
                peopleFromSomeDataSource.Add(newPerson);
            }

            await Task.Delay(2000);

            return peopleFromSomeDataSource;
        }

        // Method summary provided in interface.
        public async Task AddPerson(Person newPerson)
        {
            if (peopleFromSomeDataSource == null)
            {
                peopleFromSomeDataSource = new List<Person>();
            }
            peopleFromSomeDataSource.Add(newPerson);
            await Task.Delay(500);
        }
    }
}

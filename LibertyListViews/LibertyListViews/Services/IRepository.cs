﻿using System.Collections.Generic;
using System.Threading.Tasks;
using LibertyListViews.Models;

namespace LibertyListViews.Services
{
    public interface IRepository
    {
        /// <summary>
        /// Fetches people from our data source, which could be a database or web service of some sort.
        /// </summary>
        /// <returns>A list of Person objects.</returns>
        Task<IList<Person>> GetPeople();

        /// <summary>
        /// An overload of GetPeople to allow you ro return any number of people so you can experiment with
        /// a much longer list without having to create a hundred entries of your own.
        /// </summary>
        /// <returns>As many Person objects as you specify.</returns>
        /// <param name="numberOfPeople">Number of Person objects you'd like to retrieve.</param>
        Task<IList<Person>> GetPeople(int numberOfPeople);

        /// <summary>
        /// Adds the person to our data source. If we were using a database or web service,
        /// this is where we would add logic to save the new Person. Since our current data
        /// source is simply in memory, we just add it to our collection of People.
        /// </summary>
        /// <param name="newPerson">New person to add.</param>
        Task AddPerson(Person newPerson);
    }
}
﻿using System;
using System.Collections.Specialized;
using System.Text;

namespace LibertyListViews.Helpers
{
    public static class CustomExtensionMethods
    {
        /// <summary>
        /// Specific to the NotifyCollectionChangedEventArgs, this simply prints out the most 
        /// important information contained in this class to the Application Output pad.
        /// </summary>
        public static string PrettyPrint(this NotifyCollectionChangedEventArgs args)
        {
            var prettyStringBuilder = new StringBuilder("\n");
            prettyStringBuilder.AppendLine($"NewStartingIndex:  {args.NewStartingIndex}");
            prettyStringBuilder.AppendLine($"OldStartingIndex:  {args.OldStartingIndex}");

            if (args.NewItems != null)
            {
                prettyStringBuilder.AppendLine($"{args.NewItems?.Count} new items:");
                foreach (var item in args.NewItems)
                {
                    prettyStringBuilder.AppendLine($"\t{item.ToString()}");
                }
            }
            else
            {
                prettyStringBuilder.AppendLine($"0 new items.");
            }


            if (args.OldItems != null)
            {
                prettyStringBuilder.AppendLine($"{args.OldItems?.Count} old items:");
                foreach (var item in args.OldItems)
                {
                    prettyStringBuilder.AppendLine($"\t{item.ToString()}");
                }
            }
            else
            {
                prettyStringBuilder.AppendLine($"0 old items.");
            }

            prettyStringBuilder.AppendLine();

            return prettyStringBuilder.ToString();
        }
    }
}

﻿using System;
namespace LibertyListViews.Helpers
{
    public static class NavParameterKeys
    {
        /// <summary>
        /// This key is used with the NavigationParameters when a new person is added on the AddPersonPage.
        /// The new person is passed back to the calling page when the user taps the 'Save' button.
        /// </summary>
        public const string ADD_NEW_PERSON_KEY = "addNewPersonKey";
    }
}

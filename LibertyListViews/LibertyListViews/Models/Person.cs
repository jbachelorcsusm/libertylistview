﻿using System;
using Prism.Mvvm;

namespace LibertyListViews.Models
{
    /// <summary>
    /// Simple model class for a Person. This is the data type our ListView contains.
    /// </summary>
    public class Person : BindableBase
    {
        private string _firstName;
        public string FirstName
        {
            get { return _firstName; }
            set { SetProperty(ref _firstName, value); }
        }

        private string _lastName;
        public string LastName
        {
            get { return _lastName; }
            set { SetProperty(ref _lastName, value); }
        }

        public override string ToString()
        {
            return $"FirstName={FirstName}, LastName={LastName}";
        }
    }
}
